const express = require("express");
const app = express();

app.get("/", (req, res) => {
	res.json({
		titulo: "Programacion 1",
		alumnos: [
			{ nombre: "Nombre", apellido: "Apellido", cedula: "12346897" },
		],
	});
});

app.listen(3000, function () {
	console.log("Servidor corriendo en el puerto 3000");
});
